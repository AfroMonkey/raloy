from odoo import _, api, fields, models


class BoM(models.Model):
    _inherit = "mrp.bom"

    factor_merma = fields.Float()
    factor_homologacion = fields.Float()
    factor_analisis = fields.Float()
    total_production_cost = fields.Float(
        compute="_compute_total_production_cost",
        store=True,
    )

    @api.depends(
        "bom_line_ids.production_cost",
        "factor_merma",
        "factor_homologacion",
        "factor_analisis",
    )
    def _compute_total_production_cost(self):
        for bom in self:
            bom.total_production_cost = (
                sum(bom.bom_line_ids.mapped("production_cost"))
                + bom.factor_merma
                + bom.factor_homologacion
                + bom.factor_analisis
            )
