# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ProductTemplateCostoOtros(models.Model):
    _name = "product.template.costo_otros"

    name = fields.Char(
        string="Descripción",
        required=True,
    )
    product_tmpl_id = fields.Many2one(
        comodel_name="product.template",
        string="Producto",
        required=True,
    )
    value = fields.Float(
        string="Costo",
        required=True,
    )
    value_empaque_costeo = fields.Float(
        string="Costo Empaque Costeo",
        compute="_compute_value_empaque_costeo",
        store=True,
    )

    @api.depends("value", "product_tmpl_id.empaque_costeo")
    def _compute_value_empaque_costeo(self):
        for record in self:
            record.value_empaque_costeo = record.value * record.product_tmpl_id.empaque_costeo
