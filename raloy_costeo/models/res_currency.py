# coding=utf-8
from odoo import _, api, fields, models


class Currency(models.Model):
    _inherit = "res.currency"

    last_export_rate_id = fields.Many2one(
        comodel_name="export_currency_rate",
        string="Última tasa de exportación",
        readonly=True,
    )
