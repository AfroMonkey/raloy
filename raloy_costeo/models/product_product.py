from odoo import _, api, fields, models


class ProductProduct(models.Model):
    _inherit = "product.product"

    last_purchase_line_id = fields.Many2one(
        related="product_tmpl_id.last_purchase_line_id",
    )
    last_purchase_order_id = fields.Many2one(
        related="product_tmpl_id.last_purchase_order_id",
    )
    gastos_exportacion_unitario = fields.Float(
        related="product_tmpl_id.gastos_exportacion_unitario",
    )
    last_export_rate_id = fields.Many2one(
        related="product_tmpl_id.last_export_rate_id",
    )
    fixed_cost = fields.Float(
        related="product_tmpl_id.fixed_cost",
    )
    last_purchase_price_unit = fields.Float(
        related="product_tmpl_id.last_purchase_price_unit",
    )
    unit_cost_local_currency = fields.Float(
        related="product_tmpl_id.unit_cost_local_currency",
    )
    tambor = fields.Float(
        related="product_tmpl_id.tambor",
    )
    flete = fields.Float(
        related="product_tmpl_id.flete",
    )
    flete_amer = fields.Float(
        related="product_tmpl_id.flete_amer",
    )
    tambor_rec = fields.Float(
        related="product_tmpl_id.tambor_rec",
    )
    bom_id = fields.Many2one(
        related="product_tmpl_id.bom_id",
    )
    bom_total_production_cost = fields.Float(
        related="product_tmpl_id.bom_total_production_cost",
    )
    cost_type = fields.Selection(
        related="product_tmpl_id.cost_type",
    )
