from odoo import _, api, fields, models


class Company(models.Model):
    _inherit = "res.company"

    costo_operacion_venta = fields.Float()
    tarima = fields.Float()
    comision_vendedor = fields.Float()
    regalia_idemitsu = fields.Float()
    pago_anticipado = fields.Float()
    publicidad = fields.Float()
    tipo_cambio_exportacion = fields.Float()
    costo_tote = fields.Float()
