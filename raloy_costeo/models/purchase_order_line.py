from odoo import _, api, fields, models


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    # udv = fields.Char()
    # cantidad = fields.Float()
    flete_americano = fields.Float()
    flete_nacional = fields.Float()
    despacho_aduanal = fields.Float()
    arancel = fields.Float()
    # total = fields.Float(
    #     compute="_compute_total",
    #     store=True,
    # )
    # precio_unitario = fields.Float(
    #     compute="_compute_precio_unitario",
    #     store=True,
    # )
    # moneda = fields.Char()
    # proveedor = fields.Char()
    otros_gastos = fields.Float()
    gastos_exportacion_total = fields.Float(
        compute="_compute_gastos_exportacion_total",
        store=True,
    )
    gastos_exportacion_unitario = fields.Float(
        compute="_compute_gastos_exportacion_unitario",
        store=True,
    )

    @api.depends(
        "flete_americano",
        "flete_nacional",
        "despacho_aduanal",
        "arancel",
        "otros_gastos",
    )
    def _compute_gastos_exportacion_total(self):
        for line in self:
            line.gastos_exportacion_total = sum(
                (
                    line.flete_americano,
                    line.flete_nacional,
                    line.despacho_aduanal,
                    line.arancel,
                    line.otros_gastos,
                )
            )

    @api.depends("gastos_exportacion_total", "product_qty")
    def _compute_gastos_exportacion_unitario(self):
        for line in self:
            line.gastos_exportacion_unitario = (
                (line.gastos_exportacion_total / line.product_qty) if line.product_qty else 0.0
            )

    # registro = fields.Datetime()
    # active = fields.Boolean()
    # usuario = fields.Integer()
    # registro_update = fields.Datetime()

    # def _compute_total(self):
    #     for line in self:
    #         line.total = 0  # TODO

    #     #         sum(flete_americano
    #     # flete_nacional
    #     # despacho_aduanal
    #     # arancel)

    # def _compute_precio_unitario(self):
    #     for line in self:
    #         line.precio_unitario = 0  # TODO
    #     # total/cantidad

    def set_as_last_purchase_line_in_product(self):
        for line in self:
            line.product_id.product_tmpl_id.last_purchase_line_id = line.id
            line.product_id.product_tmpl_id.cost_type = "purchase"

    def create(self, vals):
        res = super(PurchaseOrderLine, self).create(vals)
        res.set_as_last_purchase_line_in_product()
        return res

    def write(self, vals):
        res = super(PurchaseOrderLine, self).write(vals)
        self.set_as_last_purchase_line_in_product()
        return res

    def unlink(self):
        product_tmpl_ids = self.mapped("product_id").mapped("product_tmpl_id")
        res = super(PurchaseOrderLine, self).unlink()
        for product_tmpl_id in product_tmpl_ids:
            last_line = self.search(
                [("product_id.product_tmpl_id", "=", product_tmpl_id.id)],
                order="id desc",
                limit=1,
            )
            if last_line:
                last_line.set_as_last_purchase_line_in_product()
        return res
