from odoo import _, api, fields, models


class CosteoSettings(models.TransientModel):
    _inherit = "res.config.settings"
    _name = "costeo.settings"

    company_id = fields.Many2one(
        comodel_name="res.company",
        required=True,
        default=lambda self: self.env.user.company_id,
    )
    costo_operacion_venta = fields.Float(related="company_id.costo_operacion_venta")
    tarima = fields.Float(related="company_id.tarima")
    comision_vendedor = fields.Float(related="company_id.comision_vendedor")
    regalia_idemitsu = fields.Float(related="company_id.regalia_idemitsu")
    pago_anticipado = fields.Float(related="company_id.pago_anticipado")
    publicidad = fields.Float(related="company_id.publicidad")
    tipo_cambio_exportacion = fields.Float(related="company_id.tipo_cambio_exportacion")
    costo_tote = fields.Float(related="company_id.costo_tote")
