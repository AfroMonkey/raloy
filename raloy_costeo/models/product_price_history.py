# coding=utf-8

from odoo import _, api, fields, models


class ProductPriceHistory(models.Model):
    _inherit = "product.price.history"

    product_tmpl_id = fields.Many2one(
        related="product_id.product_tmpl_id",
        store=True,
    )
