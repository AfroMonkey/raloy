# -*- coding: utf-8 -*-
from odoo import fields, models


class PackageType(models.Model):
    _name = "package_type"

    _sql_constraints = [
        (
            "name_uniq",
            "unique(name)",
            "El nombre del tipo de empaque debe ser único",
        ),
    ]

    name = fields.Selection(
        string="Nombre",
        required=True,
        selection=[
            ("BIDON", "BIDON"),
            ("BOTELLA", "BOTELLA"),
            ("CUBETA", "CUBETA"),
            ("GARRAFA", "GARRAFA"),
            ("GRANEL", "GRANEL"),
            ("OTROS", "OTROS"),
            ("TAMBOR", "TAMBOR"),
            ("TARRO", "TARRO"),
            ("TOTE", "TOTE"),
            ("TUBO", "TUBO/CARTUCHO"),
            ("CAJA", "CAJA"),
        ],
    )
    cost = fields.Float(
        string="Costo",
        required=True,
    )
