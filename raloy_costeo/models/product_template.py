# coding=utf-8

from odoo import _, api, fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    last_purchase_line_id = fields.Many2one(
        comodel_name="purchase.order.line",
        readonly=True,
    )
    last_purchase_order_id = fields.Many2one(
        comodel_name="purchase.order",
        readonly=True,
        related="last_purchase_line_id.order_id",
    )
    gastos_exportacion_unitario = fields.Float(
        related="last_purchase_line_id.gastos_exportacion_unitario",
        store=True,
    )
    last_export_rate_id = fields.Many2one(
        string="Ultimo Tipo de Cambio Costeo",
        related="last_purchase_order_id.currency_id.last_export_rate_id",
        store=True,
    )
    fixed_cost = fields.Float(
        string="Costo Fijo",
    )
    last_purchase_price_unit = fields.Float(
        related="last_purchase_line_id.price_unit",
        store=True,
    )
    unit_cost_local_currency = fields.Float(
        string="Costo Total MXN",
        compute="_compute_unit_cost_local_currency",
        store=True,
    )
    tambor = fields.Float()
    flete = fields.Float()
    flete_amer = fields.Float()
    tambor_rec = fields.Float()
    bom_id = fields.Many2one(
        comodel_name="mrp.bom",
        compute="_compute_bom_id",
    )
    bom_total_production_cost = fields.Float(
        related="bom_id.total_production_cost",
    )
    udv = fields.Float()
    profit_rate = fields.Float(
        string="Factor planta",
    )
    export_rate = fields.Float(
        string="Factor exportación",
    )
    costo_ing = fields.Float(
        string="Costo Ingeniería",
        compute="_compute_costo_ing",
        store=True,
    )
    lab_planta = fields.Float(
        string="LAB Planta",
        compute="_compute_lab_planta",
        store=True,
    )
    lab_export = fields.Float(
        string="LAB Exportación",
        compute="_compute_lab_export",
        store=True,
    )
    package_type_id = fields.Many2one(
        comodel_name="package_type",
        compute="_compute_package_type_id",
        store=True,
    )
    package_type_2 = fields.Selection(  # To have 2 fields in same view
        related="package_type",
    )
    costo_empaque = fields.Float(
        string="Costo Empaque",
        compute="_compute_costo_empaque",
        store=True,
    )
    costo_empaque_2 = fields.Float(  # To have 2 fields in same view
        related="costo_empaque",
    )
    empaque_costeo = fields.Float(
        string="Empaque para Costeo",
    )
    factor_merma = fields.Float(
        related="bom_id.factor_merma",
    )
    factor_homologacion = fields.Float(
        related="bom_id.factor_homologacion",
    )
    factor_analisis = fields.Float(
        related="bom_id.factor_analisis",
    )
    total_production_cost = fields.Float(
        related="bom_id.total_production_cost",
    )
    is_pt = fields.Boolean(
        compute="_compute_is_pt",
        store=True,
    )
    ept_bom_line_ids = fields.Many2many(
        comodel_name="mrp.bom.line",
        compute="_compute_ept_bom_line_ids",
    )
    total_ept_bom = fields.Float(
        compute="_compute_total_ept_bom",
    )
    package_bom_line_ids = fields.Many2many(
        comodel_name="mrp.bom.line",
        compute="_compute_package_bom_line_ids",
    )
    total_package_bom = fields.Float(
        compute="_compute_total_package_bom",
    )
    is_ept = fields.Boolean(
        compute="_compute_is_ept",
        store=True,
    )
    lab_planta_per_liter = fields.Float(
        string="LAB Planta x Litro",
        compute="_compute_lab_planta_per_liter",
        store=True,
    )
    x_profit_rate = fields.Float(
        related="profit_rate",
    )
    x_export_rate = fields.Float(
        related="export_rate",
    )
    x_costo_ing = fields.Float(
        related="bom_total_production_cost",
    )
    x_lab_planta = fields.Float(
        related="lab_planta",
    )
    x_lab_exp = fields.Float(
        related="lab_export",
    )
    # x_nivel_calidad = fields.Many2one(
    #     comodel_name="product.template",
    # )
    # x_servicio = fields.Many2one(
    #     comodel_name="product.template",
    # )
    costo_otros = fields.Float(
        string="Costo Otros",
        compute="_compute_costo_otros",
        store=True,
    )
    costo_otros_ids = fields.One2many(
        comodel_name="product.template.costo_otros",
        inverse_name="product_tmpl_id",
    )
    origen = fields.Char(
        compute="_compute_origen",
    )
    cost_type = fields.Selection(
        selection=[
            ("ept", "EPT"),
            ("fixed", "Fixed"),
            ("historic", "Historic"),
            ("purchase", "Purchase"),
        ],
        compute="_compute_cost_type",
        store=True,
        default="historic",
    )
    final_package_unit_cost = fields.Float(
        string="Costo Unitario Empaque",
        compute="_compute_final_package_unit_cost",
        store=True,
    )
    package_type_id_cost = fields.Float(
        related="package_type_id.cost",
    )

    @api.depends(
        "categ_id.display_name",
        "fixed_cost",
    )
    def _compute_cost_type(self):
        for template in self:
            if template.is_ept:
                template.cost_type = "ept"
                continue
            if template.fixed_cost:
                template.cost_type = "fixed"
                continue
            template.cost_type = "historic"

    @api.depends("categ_id.display_name")
    def _compute_origen(self):
        for template in self:
            if template.is_ept:
                template.origen = "EPT"
                continue
            origen = template.categ_id.display_name.split(" / ")
            template.origen = origen[1] if len(origen) > 1 else origen[0]

    @api.depends("costo_otros_ids.value", "empaque_costeo")
    def _compute_costo_otros(self):
        for template in self:
            costo = sum(template.costo_otros_ids.mapped("value_empaque_costeo"))
            template.costo_otros = costo

    @api.depends("lab_planta", "udv")
    def _compute_lab_planta_per_liter(self):
        for template in self:
            template.lab_planta_per_liter = (
                template.lab_planta / template.udv if template.udv else 0
            )

    @api.depends("categ_id.display_name")
    def _compute_is_ept(self):
        for template in self:
            categ = template.categ_id.display_name or ""
            categ_first_level = categ.split("/")[0].strip()
            template.is_ept = categ_first_level == "EPT"

    @api.depends("ept_bom_line_ids")
    def _compute_total_ept_bom(self):
        for template in self:
            template.total_ept_bom = sum(template.ept_bom_line_ids.mapped("production_cost"))

    @api.depends("package_bom_line_ids")
    def _compute_total_package_bom(self):
        for template in self:
            template.total_package_bom = sum(
                template.package_bom_line_ids.mapped("production_cost")
            )

    @api.depends("bom_id")
    def _compute_ept_bom_line_ids(self):
        for record in self:
            if not record.bom_id:
                record.ept_bom_line_ids = False
                continue
            if not record.bom_id.bom_line_ids:
                record.ept_bom_line_ids = False
                continue
            ept_product = None
            for line in record.bom_id.bom_line_ids:
                if line.product_tmpl_id.is_ept:
                    ept_product = line.product_tmpl_id
                    break
            if not (ept_product and ept_product.bom_id):
                record.ept_bom_line_ids = False
                continue
            record.ept_bom_line_ids = ept_product.bom_id.bom_line_ids

    @api.depends("bom_id")
    def _compute_package_bom_line_ids(self):
        for record in self:
            record.package_bom_line_ids = (record.bom_id and record.bom_id.bom_line_ids) or False

    @api.depends("categ_id")
    def _compute_is_pt(self):
        for record in self:
            if not record.categ_id:
                record.is_pt = False
                continue
            name = record.categ_id.display_name
            record.is_pt = name.startswith("PT")

    @api.depends("bom_total_production_cost", "costo_empaque", "costo_otros")
    def _compute_costo_ing(self):
        for template in self:
            template.costo_ing = (
                template.bom_total_production_cost + template.costo_empaque + template.costo_otros
            )

    @api.depends("costo_ing", "profit_rate")
    def _compute_lab_planta(self):
        for template in self:
            if not template.profit_rate:
                template.lab_planta = 0
                continue
            template.lab_planta = template.costo_ing / template.profit_rate

    @api.depends("costo_ing", "export_rate")
    def _compute_lab_export(self):
        for template in self:
            reduce_rate = 1 - (
                template.company_id.comision_vendedor
                + template.company_id.regalia_idemitsu
                + template.company_id.pago_anticipado
                + template.company_id.publicidad
            )
            if not all(
                (template.export_rate, template.udv, reduce_rate, template.last_export_rate_id.rate)
            ):
                template.lab_export = 0
                continue
            template.lab_export = template.costo_ing / template.export_rate
            template.lab_export /= template.udv
            template.lab_export += (
                template.company_id.costo_operacion_venta
                + template.company_id.tarima
                + template.company_id.costo_tote
            )
            template.lab_export /= reduce_rate
            template.lab_export *= template.udv
            template.lab_export /= template.last_export_rate_id.rate

    @api.depends("empaque_costeo", "udv")
    def _compute_final_package_unit_cost(self):
        for template in self:
            template.final_package_unit_cost = template.empaque_costeo or template.udv

    @api.depends("package_type_id_cost", "final_package_unit_cost")
    def _compute_costo_empaque(self):
        for template in self:
            template.costo_empaque = (
                template.package_type_id_cost * template.final_package_unit_cost
            )

    @api.depends("bom_ids")
    def _compute_bom_id(self):
        for template in self:
            template.bom_id = template.bom_ids and template.bom_ids[0]

    def _get_last_history_cost(self):
        price = self.env["product.price.history"].search(
            [("product_tmpl_id", "=", self.id)],
            order="create_date desc",
            limit=1,
        )
        return price and price.cost or 0

    def _get_cost_last_purchase(self):
        return (
            (
                self.last_purchase_price_unit * self.last_export_rate_id.rate
                + self.gastos_exportacion_unitario
            )
            + self.tambor
            + self.flete
            + self.flete_amer
            - self.tambor_rec
        )

    @api.depends(
        "last_purchase_price_unit",
        "last_export_rate_id.rate",
        "tambor",
        "flete",
        "flete_amer",
        "tambor_rec",
        "cost_type",
    )
    def _compute_unit_cost_local_currency(self):
        for template in self:
            if template.cost_type == "fixed":
                cost = template.fixed_cost
            elif template.cost_type == "historic":
                cost = template._get_last_history_cost()
            elif template.cost_type == "purchase":
                cost = template._get_cost_last_purchase()
            else:
                cost = 0  # TODO: raise error
            template.unit_cost_local_currency = cost

    @api.depends("package_type")
    def _compute_package_type_id(self):
        for template in self:
            template.package_type_id = self.env["package_type"].search(
                [("name", "=", template.package_type)], limit=1
            )
