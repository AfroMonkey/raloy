from odoo import _, api, fields, models


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    costeo_order_line_ids = fields.One2many(
        related="order_line",
    )
    last_export_rate_id = fields.Many2one(
        related="currency_id.last_export_rate_id",
    )

    # @api.multi
    # def button_confirm(self):
    #     res = super(PurchaseOrder, self).button_confirm()
    #     self.set_last_purchase_line_id_in_products()
    #     return res

    # button_done

    @api.multi
    def set_last_purchase_line_id_in_products(self):
        for order in self:
            order.order_line.set_as_last_purchase_line_in_product()
