from odoo import _, api, fields, models
from odoo.addons import decimal_precision as dp


class BoM(models.Model):
    _inherit = "mrp.bom.line"

    production_unit_cost = fields.Float(
        compute="_compute_production_unit_cost",
        store=True,
    )
    production_cost = fields.Float(
        compute="_compute_production_cost",
        store=True,
    )
    product_tmpl_id = fields.Many2one(
        related="product_id.product_tmpl_id",
        store=True,
    )
    formula_p = fields.Float(
        "% de Formula",
        digits=dp.get_precision("Product Unit of Measure"),
        track_visibility="onchange",
    )
    material_type = fields.Selection(
        related="product_id.material_type",
        string="Tipo",
    )
    origen = fields.Char(
        related="product_id.origen",
    )
    cost_type = fields.Selection(
        related="product_id.cost_type",
    )

    @api.depends(
        "product_tmpl_id.unit_cost_local_currency",
        "product_tmpl_id.categ_id",
        "product_tmpl_id.bom_total_production_cost",
    )
    def _compute_production_unit_cost(self):
        for line in self:
            if line.product_tmpl_id.is_ept:
                line.production_unit_cost = line.product_tmpl_id.bom_total_production_cost
                continue
            line.production_unit_cost = line.product_tmpl_id.unit_cost_local_currency

    @api.depends("product_qty", "production_unit_cost")
    def _compute_production_cost(self):
        for line in self:
            line.production_cost = line.product_qty * line.production_unit_cost
