from odoo import _, api, fields, models


class ExportCurrencyRate(models.Model):
    _name = "export_currency_rate"

    name = fields.Char(
        compute="_compute_name",
        store=True,
    )
    date = fields.Date(
        string="Fecha",
        required=True,
        index=True,
    )
    currency_id = fields.Many2one(
        comodel_name="res.currency",
        string="Moneda",
        required=True,
        index=True,
    )
    rate = fields.Float(
        string="Tasa (en MXN)",
        required=True,
    )

    @api.depends("currency_id", "date", "rate")
    def _compute_name(self):
        for record in self:
            record.name = "{}-{}({})".format(record.currency_id.name, record.date, record.rate)

    @api.model
    def create(self, vals):
        res = super(ExportCurrencyRate, self).create(vals)
        if res.currency_id.name == "MXN":
            res.rate = 1.0
        if (
            not res.currency_id.last_export_rate_id
            or res.date > res.currency_id.last_export_rate_id.date
        ):
            res.currency_id.last_export_rate_id = res.id
        return res
