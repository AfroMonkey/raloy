{
    "name": "Raloy Costeo",
    "version": "10.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "mrp",
        "purchase",
        "raloy_product_package_type",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/menu.xml",
        "views/export_currency_rate.xml",
        "views/mrp_bom.xml",
        "views/mrp_config_settings.xml",
        "views/package_type.xml",
        "views/product_product.xml",
        "views/product_template_costo_otros.xml",
        "views/product_template.xml",
        "views/purchase_order.xml",
        "views/res_currency.xml",
    ],
}
