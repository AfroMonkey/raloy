{
    "name": "Raloy Product Package Type",
    "version": "1.1",
    "author": "René Villegas",
    "website": "https://about.me/rene.villegas",
    "depends": [
        "product",
        "raloy_product_pieces_fields",
    ],
    "data": [
        "views/product_template.xml",
    ],
}
