from odoo import api, fields, models


class ProductPackageType(models.Model):
    _inherit = 'product.template'

    package_type = fields.Selection(
        string="Package Type",
        selection=[
            ('BIDON', 'BIDON'),
            ('BOTELLA', 'BOTELLA'),
            ('CUBETA', 'CUBETA'),
            ('GARRAFA', 'GARRAFA'),
            ('GRANEL', 'GRANEL'),
            ('OTROS', 'OTROS'),
            ('TAMBOR', 'TAMBOR'),
            ('TARRO', 'TARRO'),
            ('TOTE', 'TOTE'),
            ('TUBO', 'TUBO/CARTUCHO'),
	    ('CAJA', 'CAJA')
        ]
    )

    material_type = fields.Selection(
        string="Material Type",
        selection=[
            ('ACEITE', 'ACEITE'),
            ('ACIDO', 'ACIDO'),
            ('ADITIVO', 'ADITIVO'),
            ('AGUA', 'AGUA'),
            ('ANTIESPUMANTE', 'ANTIESPUMANTE'),
            ('ASFALTO', 'ASFALTO'),
            ('BASE', 'BASE'),
            ('BIDON', 'BIDON'),
            ('BOLSA', 'BOLSA'),
            ('BOTELLA', 'BOTELLA'),
            ('CAJA', 'CAJA'),
            ('CINTA', 'CINTA'),
            ('CUBETA', 'CUBETA'),
            ('DISPLAY', 'DISPLAY'),
            ('ESQUINERO', 'ESQUINERO'),
            ('ETIQUETA', 'ETIQUETA'),
            ('FLEJE', 'FLEJE'),
            ('GARRAFA', 'GARRAFA'),
            ('HUACAL', 'HUACAL'),
            ('LOGO', 'LOGO'),
            ('MANGA', 'MANGA'),
            ('MEJORADOR', 'MEJORADOR'),
            ('PIGMENTO', 'PIGMENTO'),
            ('PLAYO', 'PLAYO'),
            ('PORRON', 'PORRON'),
            ('QUIMICO', 'QUIMICO'),
            ('SELLO', 'SELLO'),
            ('SEPARADOR', 'SEPARADOR'),
            ('SOLVENTE', 'SOLVENTE'),
            ('TAMBOR', 'TAMBOR'),
            ('TAPA', 'TAPA'),
            ('TARIMA', 'TARIMA'),
            ('TARRO', 'TARRO'),
            ('TOTE', 'TOTE'),
        ]
    )
